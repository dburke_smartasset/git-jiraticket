# git-jiraticket

When creating a branch with git, I give it a name that includes the JIRA ticket ID in it. Usually, this will just be the JIRA ticket ID, but sometimes it might be a hotfix (so I give it a prefix of `hotfix_`) or sometimes, I'll need to make multiple merge requests on the same ticket and have to add suffixes to the ticket ID (`SRE-251_revert_to_clb`, e.g.).

When I'm typing a git commit message, I want a quick way to get just the JIRA ticket ID in the message. That's where this snippet comes in. Place this file somewhere in your `$PATH`, and you now have a new git subcommand, `jiraticket`. For example:
```
git commit -m "$(git jiraticket) This commit message will now have the JIRA ticket ID at the start of
the commit message automagically"
```
If that feels risky to you,
```
T=$(git jiraticket)
echo ${T}
git commit -m "${T} commit message here..."
```
